import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class GUI_Semaforo extends JFrame {

	private JPanel contentPane;
	ProductorConsumidor pc = new ProductorConsumidor();
	int buffer, ap, ac, tp, tc;
	
	private JTextField textBuffer;
	private JTextField textArticulosProducir;
	private JButton btnNewButton_1;
	private JButton btnNewButton_3;
	private JTextField textTiempoProductor;
	private JTextField textTiempoConsumidor;
	private JButton btnNewButton_4;
	private JTextField textArticulosConsumir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_Semaforo frame = new GUI_Semaforo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_Semaforo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 643, 543);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textBuffer = new JTextField();
		textBuffer.setBounds(319, 11, 86, 20);
		contentPane.add(textBuffer);
		textBuffer.setColumns(10);
		
		textArticulosProducir = new JTextField();
		textArticulosProducir.setBounds(319, 144, 86, 20);
		contentPane.add(textArticulosProducir);
		textArticulosProducir.setColumns(10);
		
		JButton btnNewButton = new JButton("ENVIAR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buffer = Integer.parseInt(textBuffer.getText());
				pc.get_Buffer(buffer);
			}
		});
		btnNewButton.setBounds(204, 10, 105, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_2 = new JButton("ENVIAR");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ap = Integer.parseInt(textArticulosProducir.getText());
				pc.get_ap(ap);
			}
		});
		btnNewButton_2.setBounds(204, 143, 105, 23);
		contentPane.add(btnNewButton_2);
		
		btnNewButton_1 = new JButton("ENVIAR");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tp = Integer.parseInt(textTiempoProductor.getText());
				pc.get_tp(tp);
			}
		});
		btnNewButton_1.setBounds(204, 75, 105, 23);
		contentPane.add(btnNewButton_1);
		
		btnNewButton_3 = new JButton("ENVIAR");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tc = Integer.parseInt(textTiempoConsumidor.getText());
				pc.get_tc(tc);
			}
		});
		btnNewButton_3.setBounds(204, 109, 105, 23);
		contentPane.add(btnNewButton_3);
		
		textTiempoProductor = new JTextField();
		textTiempoProductor.setBounds(319, 76, 86, 20);
		contentPane.add(textTiempoProductor);
		textTiempoProductor.setColumns(10);
		
		textTiempoConsumidor = new JTextField();
		textTiempoConsumidor.setBounds(319, 110, 86, 20);
		contentPane.add(textTiempoConsumidor);
		textTiempoConsumidor.setColumns(10);
		
		textArticulosConsumir = new JTextField();
		textArticulosConsumir.setBounds(319, 42, 86, 20);
		contentPane.add(textArticulosConsumir);
		textArticulosConsumir.setColumns(10);
		
		btnNewButton_4 = new JButton("ENVIAR");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ac = Integer.parseInt(textArticulosConsumir.getText());
				pc.get_ac(ac);
			}
		});
		btnNewButton_4.setBounds(204, 44, 105, 23);
		contentPane.add(btnNewButton_4);
		
		JLabel lblNewLabel = new JLabel("INGRESA EL TAMA\u00D1O DEL BUFFER");
		lblNewLabel.setBounds(10, 11, 188, 20);
		contentPane.add(lblNewLabel);
		
		JLabel lblArticulosAConsumir = new JLabel("ARTICULOS A CONSUMIR");
		lblArticulosAConsumir.setBounds(10, 44, 188, 20);
		contentPane.add(lblArticulosAConsumir);
		
		JLabel lblTiempoEsperaProductor = new JLabel("TIEMPO ESPERA PRODUCTOR");
		lblTiempoEsperaProductor.setBounds(10, 75, 188, 20);
		contentPane.add(lblTiempoEsperaProductor);
		
		JLabel lblTiempoEsperaConsumidor = new JLabel("TIEMPO ESPERA CONSUMIDOR");
		lblTiempoEsperaConsumidor.setBounds(10, 109, 188, 20);
		contentPane.add(lblTiempoEsperaConsumidor);
		
		JLabel lblArticulosAProducir = new JLabel("ARTICULOS A PRODUCIR");
		lblArticulosAProducir.setBounds(10, 144, 188, 20);
		contentPane.add(lblArticulosAProducir);
		

		
		
	}
}
