import java.util.concurrent.*;

class BufferLimitado
{
	double buffer[];
	int inBuf= 0, outBuf=0;
	Semaphore mutex = new Semaphore(1, true);
	Semaphore isEmpty = new Semaphore(0, true);
	Semaphore isFull;
	
	public BufferLimitado (int tam)
	{
		buffer = new double[tam];
		 isFull = new Semaphore(buffer.length, true);
	}
	
	public void deposit(double value) throws InterruptedException
	{
		isFull.acquire(); 
			mutex.acquire(); 
				buffer[inBuf] = value;
				inBuf= (inBuf+ 1) % buffer.length;
			mutex.release();
		isEmpty.release(); 
	}
	
	public double fetch() throws InterruptedException
	{
		double value;
		isEmpty.acquire(); 
			mutex.acquire(); 
				value = buffer[outBuf]; 
				outBuf= (outBuf+1) % buffer.length;
			mutex.release();
		isFull.release(); 
		return value;
	}
}